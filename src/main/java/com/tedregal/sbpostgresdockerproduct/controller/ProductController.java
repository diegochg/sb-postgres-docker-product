package com.tedregal.sbpostgresdockerproduct.controller;

import com.tedregal.sbpostgresdockerproduct.model.Product;
import com.tedregal.sbpostgresdockerproduct.repository.ProductRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/products")
public class ProductController {

  @Autowired
  private ProductRepository productRepository;

  @GetMapping("/all")
  public ResponseEntity<List<Product>> getAllProducts() {
    return new ResponseEntity<List<Product>>(productRepository.findAll(), HttpStatus.OK);
  }

  @PostMapping("/new")
  public ResponseEntity<String> saveProduct(@RequestBody Product product){
    productRepository.save(product);
    return new ResponseEntity<String>("Product created", HttpStatus.CREATED);
  }

}
