package com.tedregal.sbpostgresdockerproduct.repository;

import com.tedregal.sbpostgresdockerproduct.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

}
