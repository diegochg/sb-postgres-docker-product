package com.tedregal.sbpostgresdockerproduct;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SbPostgresDockerProductApplication {

	public static void main(String[] args) {
		SpringApplication.run(SbPostgresDockerProductApplication.class, args);
	}

}
