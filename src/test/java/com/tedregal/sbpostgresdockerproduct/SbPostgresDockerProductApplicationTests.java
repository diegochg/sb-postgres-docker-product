package com.tedregal.sbpostgresdockerproduct;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = SbPostgresDockerProductApplicationTests.class)
class SbPostgresDockerProductApplicationTests {

	@Test
	void contextLoads() {
	}

}
